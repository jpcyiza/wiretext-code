## 1.2.1 (2023-06-02)

### Documentation

* Updated installation procedures.
* Fixed visual issues in VSCode documentation view.


## 1.2.0 (2023-06-01)

### Breaking changes

* Notes no longer render as Markdown, but as a more limited form of rich text. (See features below.)
* Text node strings are no longer interpreted as plain text, but as rich text. (See features below.)

### Features

* Limited Markdown-like rich text can now be used in text nodes. In particular: *em*, **strong**, `code`, paragraphs (separated by two newlines) and lists (with single-line items) are supported.
* The example WireText files (used to generate the README.md) now look decent when opened with the extension.

### Bug fixes

* Fixed broken links between screens
* Fixed visual glitches in README.md


## 1.1.4 (2023-06-01)

### Features

* Prepared the extension for the Marketplace

### Bug fixes

* 'Inconsistent capture' is now a regular (useful) parse error
* Actually fixed focus stealing this time


## 1.1.3 (2023-05-30)

### Features

* Added 'Watch' to component library
* Show an error on duplicate properties
* Allow the use of relative URLs for images and other resources

### Bug fixes

* Fix annoying panel creation haviour
